import React from 'react'
import Link from 'next/link'
import avatarImage from '../images/avatar.png'
import { useRouter } from 'next/router'

const SideBar = () => {

    const router = useRouter();

    return (
        <aside className="bg-gray-800 sm:w-1/3 xl:w-1/5 sm:min-h-screen p-5">
            <div className="mb-3">
                <p className="text-center text-white text-3xl font-black">Axosnet</p>
            </div>
            <div className="mt-4">
                <img src={avatarImage} alt="avatar" id="avatarImage" />
            </div>
            <nav className="mt-8 list-none">
                <li className={router.pathname === "/" ? "bg-teal-500 p-2" : "p-2"}>
                    <Link href="/">
                        <a className="text-white mb-3 block">
                            Recibos
                        </a>
                    </Link>
                </li>
                <li className={router.pathname === "/providers" ? "bg-teal-500 p-2" : "p-2"}>
                    <Link href="/providers">
                        <a className="text-white mb-3 block">
                            Proveedores
                        </a>
                    </Link>
                </li>
                <li className={router.pathname === "/currencies" ? "bg-teal-500 p-2" : "p-2"}>
                    <Link href="/currencies">
                        <a className="text-white mb-3 block">
                            Divisas
                        </a>
                    </Link>
                </li>
            </nav>
        </aside>
    )
}

export default SideBar