import React from 'react'
import Layout from '../components/Layout'

const Providers = () => {
    return(
        <div>
            <Layout>
                <h1 className="text-3xl text-gray-800 font-light">Proveedores</h1>
            </Layout>
        </div>
    )
}

export default Providers