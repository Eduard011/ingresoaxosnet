import React from 'react'
import Layout from './../components/Layout'
import avatarImage from './../images/avatar.png'

const Login = () => {
    return (
        <Layout>
            <div className="text-center text-2xl text-white font-black">
                <h1>¡Welcome to Axosnet!</h1>
            </div>
            <div className="flex justify-center"><img src={avatarImage} alt="avatar" id="avatarImage" width="200" height="200" /></div>
            <div className="flex justify-center mt-3">
                <div className="w-full max-w-sm">
                    <form className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4">
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">Username: </label>
                            <input 
                                className="shadow appearence-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                                id="username"
                                type="email"
                                placeholder="example@example.com"
                            />
                        </div>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">Password: </label>
                            <input 
                                className="shadow appearence-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                                id="password"
                                type="password"
                                placeholder="Enter your password"
                            />
                        </div>
                        <input 
                            type="submit"
                            className="bg-teal-500 w-full mt-5 p-2 text-white hover:bg-teal-700 rounded"
                            value="Sign In"                        />
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default Login