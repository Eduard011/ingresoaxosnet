import Layout from './../components/Layout'

const Index = () => (
  <div>
    <Layout>
      <h1 className="text-3xl text-gray-800 font-light">Administrador de Recibos</h1>
    </Layout>
  </div>
)

export default Index
